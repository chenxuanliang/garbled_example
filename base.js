const path = require('path')
const fs = require('fs')

const defaultSettings = require('./defaults')

module.exports = {
    devServer: {
        contentBase: './src/',
        historyApiFallback: true,
        hot: true,
        port: defaultSettings.port,
        publicPath: defaultSettings.publicPath,
        noInfo: false,
        setup: function(app) {
            app.get('/assets/dll.vendor.js', function(req, res) {
                res.send(fs.readFileSync(path.join(__dirname, '../dist/assets/dll.vendor.js'), 'utf8'));
            });
        },
        proxy: {
            '/api/*': {
                target: 'http://172.16.116.47:8226',
            },
        },
    },
    module: {},
    resolve: {
        enforceExtension: false,
        extensions: ['.js', '.jsx'],
        alias: {
            components: `${path.join(__dirname, '../src/components')}`,
            constants: `${path.join(__dirname, '../src/constants')}`,
            containers: `${path.join(__dirname, '../src/containers')}`,
            reducers: `${path.join(__dirname, '../src/reducers')}`,
            sagas: `${path.join(__dirname, '../src/sagas')}`,
            common: `${path.join(__dirname, '../src/common')}`,
        }
    },
}

